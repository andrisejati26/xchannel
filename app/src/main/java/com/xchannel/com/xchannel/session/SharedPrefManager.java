package com.xchannel.com.xchannel.session;

import android.content.Context;
import android.content.SharedPreferences;

import com.xchannel.com.xchannel.R;

public class SharedPrefManager {
    private static final String APP_NAME = String.valueOf(R.string.app_name);

    public static final int GOOGLE_METHOD = 200;
    public static final int FACEBOOK_METHOD = 201;
    public static final int PHONE_METHOD = 202;
    public static final int GUEST_METHOD = 203;

    public static final String NAME_KEY = "name";
    public static final String EMAIL_KEY = "email";
    public static final String PHOTO_URL_KEY = "photourl";
    public static final String PHONE_NUMBER_KEY = "phonenumber";
    public static final String METHOD_KEY = "method";
    public static final String IS_SIGNEDIN_KEY = "issignedin";

    private static SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    public SharedPrefManager(Context context) {
        sharedPreferences = context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void saveSharedPrefString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void saveSharedPrefInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public void saveSharedPrefBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getName() {
        return sharedPreferences.getString(NAME_KEY, "");
    }

    public String getEmail() {
        return sharedPreferences.getString(EMAIL_KEY, "");
    }

    public String getPhotoUrl() {
        return sharedPreferences.getString(PHOTO_URL_KEY, "");
    }

    public String getPhoneNumber() {
        return sharedPreferences.getString(PHONE_NUMBER_KEY, "");
    }

    public int getMethod() {
        return sharedPreferences.getInt(METHOD_KEY, 203);
    }

    public boolean getIsSignedIn() {
        return sharedPreferences.getBoolean(IS_SIGNEDIN_KEY, true);
    }
}
