package com.xchannel.com.xchannel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.model.RadioSchedule;

import java.util.List;

public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.ItemViewHolder> {
    private static List<RadioSchedule> dataList;
    private final Context context;
    private final ClickHandler mClickHandler;

    public RadioAdapter(Context ctx, List<RadioSchedule> data, ClickHandler clickHandler) {
        context = ctx;
        dataList = data;
        mClickHandler = clickHandler;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView hari;
        private final TextView judul;
        private final TextView jam;

        ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            hari = itemView.findViewById(R.id.hari);
            judul = itemView.findViewById(R.id.judul);
            jam = itemView.findViewById(R.id.jam);

        }

        @Override
        public void onClick(View view) {
            if (mClickHandler != null) mClickHandler.onItemClicked(getAdapterPosition());
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_siaran, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.hari.setText(dataList.get(position).getHariSiaran());
        holder.judul.setText(dataList.get(position).getJudulSiaran());
        holder.jam.setText(dataList.get(position).getWaktuSiaran());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
