package com.xchannel.com.xchannel.ui.fragment;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.adapter.VideoAdapter;
import com.xchannel.com.xchannel.adapter.SiaranAdapter;
import com.xchannel.com.xchannel.model.Live;
import com.xchannel.com.xchannel.model.Siaran;
import com.xchannel.com.xchannel.model.Video;
import com.xchannel.com.xchannel.networking.GetDataService;
import com.xchannel.com.xchannel.networking.RetrofitClientInstance;
import com.xchannel.com.xchannel.services.RadioService;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class LivestreamFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private SiaranAdapter siaranAdapter;
    private RecyclerView videoRecyclerView;
    private RecyclerView siaranRecyclerView;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    // Thumbnail Video
    private ImageView thumbVideo;

    // Youtube Player
    private YouTubePlayer mYouTubePlayer;
    private FrameLayout youtubeFrameLayout;

    // Video Player
    private VideoView videoView;

    public LivestreamFragment() { }

    public LivestreamFragment(int position) {
        int wizard_page_position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layout_id = R.layout.fragment_livestream;
        View view = inflater.inflate(layout_id, container, false);

        videoRecyclerView = view.findViewById(R.id.recyclerViewVideo);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        videoRecyclerView.setLayoutManager(layoutManager1);

        thumbVideo = view.findViewById(R.id.thumb_video);
        //thumbVideo.setVisibility(View.GONE);
        videoView = view.findViewById(R.id.view_video_server);
        youtubeFrameLayout = view.findViewById(R.id.view_video_youtube);

        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 1);
        siaranRecyclerView = view.findViewById(R.id.recyclerViewSiaran);
        siaranRecyclerView.setLayoutManager(lLayout);
        siaranRecyclerView.setAdapter(siaranAdapter);
        siaranRecyclerView.setNestedScrollingEnabled(false);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayoutVideo);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                loadStream();
                loadData();
                loadDataSiaran();
            }
        });

        mSwipeRefreshLayout.setRefreshing(false);

        return view;
    }

    private void loadStream() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<Live>> call = service.getAllStream();
        call.enqueue(new Callback<List<Live>>() {
            @Override
            public void onResponse(Call<List<Live>> call, Response<List<Live>> response) {
                // playYoutubeVideo(response.body().get(0).getUrlStream());
                setYoutubeThumbnail(response.body().get(0).getUrlStream());
            }

            @Override
            public void onFailure(Call<List<Live>> call, Throwable t) {

            }
        });
    }

    private void setYoutubeThumbnail(final String url) {
        final String id = getYoutubeId(url);

        Glide.with(getContext())
                .load("https://img.youtube.com/vi/" + id + "/0.jpg")
                .centerCrop()
                .thumbnail(0.01f)
                .into(thumbVideo);

        thumbVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playYoutubeVideo(url);
            }
        });
    }

    private void loadData() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<Video>> call = service.getAllVideo();
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                // Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadDataSiaran() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);
        Call<List<Siaran>> call = service.getAllSiaran();
        call.enqueue(new Callback<List<Siaran>>() {
            @Override
            public void onResponse(Call<List<Siaran>> call, Response<List<Siaran>> response) {
                generateDataListSiaran(response.body());
            }

            @Override
            public void onFailure(Call<List<Siaran>> call, Throwable t) {
                // if failed
            }
        });
    }

    private void generateDataListSiaran(final List<Siaran> dataList) {
        siaranAdapter = new SiaranAdapter(getActivity(), dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {
                Toast.makeText(getActivity(), "Cooming Soon" + dataList.get(position).getJudulSiaran(), Toast.LENGTH_SHORT).show();
            }
        });

        siaranRecyclerView.setAdapter(siaranAdapter);
    }

    private void generateDataList(final List<Video> dataList) {
        VideoAdapter videoAdapter = new VideoAdapter(getActivity(), dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {
                // Toast.makeText(getActivity(), "Id Video = " + dataList.get(position).getIdVideo(), Toast.LENGTH_SHORT).show();

                String urlVideo = dataList.get(position).getUrlVideo();

                switch (dataList.get(position).getVideoType()) {
                    case "server":
                        playServerVideo(urlVideo);
                        break;
                    case "youtube":
                        playYoutubeVideo(urlVideo);
                        break;
                }

            }
        });

        videoRecyclerView.setAdapter(videoAdapter);
    }

    private void playServerVideo(String urlVideo) {

        getContext().stopService(new Intent(getActivity(), RadioService.class));

        youtubeFrameLayout.setVisibility(View.GONE);
        thumbVideo.setVisibility(View.GONE);

        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoPath(urlVideo);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                MediaController mediaController = new MediaController(getActivity());
                videoView.setMediaController(mediaController);
                mediaController.setAnchorView(videoView);
            }
        });

        videoView.start();
    }

    private void playYoutubeVideo(String urlVideo) {

        getContext().stopService(new Intent(getActivity(), RadioService.class));

        thumbVideo.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);

        youtubeFrameLayout.setVisibility(View.VISIBLE);

        final String videoId = getYoutubeId(urlVideo);

        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.view_video_youtube, youTubePlayerFragment);
        transaction.commit();

        youTubePlayerFragment.initialize(Constant.API_KEY_YOUTUBE, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    mYouTubePlayer = youTubePlayer;
                    mYouTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    mYouTubePlayer.loadVideo(videoId);
                    mYouTubePlayer.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                // Youtube Error
                String errorMessage = youTubeInitializationResult.toString();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.d("errorMessage", errorMessage);
            }
        });
    }

    private static String getYoutubeId(String url) {
        final String expression = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        if (url == null || url.trim().length() <= 0) return null;

        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(url);
        try {
            if (matcher.find()) return matcher.group();
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRefresh() {
        loadStream();
        loadData();
        loadDataSiaran();
    }
}
