package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Live {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title_stream")
    @Expose
    private String titleStream;
    @SerializedName("url_stream")
    @Expose
    private String urlStream;

    public String getId() {
        return id;
    }

    public String getTitleStream() {
        return titleStream;
    }

    public String getUrlStream() {
        return urlStream;
    }
}
