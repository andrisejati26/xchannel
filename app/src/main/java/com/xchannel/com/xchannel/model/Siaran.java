package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Siaran {

    @SerializedName("id_live_video")
    @Expose
    private String idLiveVideo;
    @SerializedName("judul_siaran")
    @Expose
    private String judulSiaran;
    @SerializedName("hari_siaran")
    @Expose
    private String hariSiaran;
    @SerializedName("jam_siaran")
    @Expose
    private String jamSiaran;
    @SerializedName("deskripsi_siaran")
    @Expose
    private String deskripsiSiaran;

    public String getIdLiveVideo() {
        return idLiveVideo;
    }

    public String getJudulSiaran() {
        return judulSiaran;
    }

    public String getHariSiaran() {
        return hariSiaran;
    }

    public String getJamSiaran() {
        return jamSiaran;
    }

    public String getDeskripsiSiaran() {
        return deskripsiSiaran;
    }
}
