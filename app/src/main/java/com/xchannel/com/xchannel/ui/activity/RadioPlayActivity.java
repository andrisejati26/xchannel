package com.xchannel.com.xchannel.ui.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.adapter.RadioAdapter;
import com.xchannel.com.xchannel.model.RadioSchedule;
import com.xchannel.com.xchannel.networking.GetDataService;
import com.xchannel.com.xchannel.networking.RetrofitClientInstance;
import com.xchannel.com.xchannel.services.PlaybackStatus;
import com.xchannel.com.xchannel.services.RadioManager;
import com.xchannel.com.xchannel.services.RadioService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RadioPlayActivity extends AppCompatActivity {
    private ImageView buttonAudio;
    private RadioManager radioManager;
    private String streamURL;
    private RecyclerView mRecyclerView;
    private String mPathRadio;
    private ImageView mThumbSiaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radioplay);

        GridLayoutManager lLayout = new GridLayoutManager(this, 1);

        mThumbSiaran = (ImageView) findViewById(R.id.thumb_siaran);

        mRecyclerView = (RecyclerView) findViewById(R.id.radio_schedule);
        mRecyclerView.setLayoutManager(lLayout);
        mRecyclerView.setNestedScrollingEnabled(false);

        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        radioManager = RadioManager.with(this);

        Intent intent = getIntent();
        streamURL = intent.getStringExtra("streamurl");
        mPathRadio = intent.getStringExtra("pathradio");

        buttonAudio = findViewById(R.id.buttonAudio);
        buttonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioManager.playOrPause(streamURL);
            }
        });

        loadData();

        startRadio();

    }

    private void loadData() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<RadioSchedule>> call = service.getAllRadioSchedule("http://202.78.200.228/xchannel_apps/api/api.php?" + mPathRadio);
        call.enqueue(new Callback<List<RadioSchedule>>() {
            @Override
            public void onResponse(Call<List<RadioSchedule>> call, Response<List<RadioSchedule>> response) {
                for (int i = 0; i < response.body().size(); i++) {

                    boolean isTiming = checkTimeInRange(response.body().get(i).getWaktuSiaran());

                    if (isTiming) {
                        mPathRadio = mPathRadio.replace("radio", "siaran");
                        Glide.with(RadioPlayActivity.this)
                                .load(Constant.BASE_URL_IMAGE_THUMBNAIL + mPathRadio + "/" + response.body().get(i).getThumbSiaran())
                                .thumbnail(0.01f)
                                .into(mThumbSiaran);

                        Log.i("pathurlradio", Constant.BASE_URL_IMAGE_THUMBNAIL + mPathRadio + "/" + response.body().get(i).getThumbSiaran());
                        break;
                    }
                }

                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<RadioSchedule>> call, Throwable t) {
                 Toast.makeText(RadioPlayActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void generateDataList(final List<RadioSchedule> dataList) {

        RadioAdapter mAdapter = new RadioAdapter(RadioPlayActivity.this, dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {
                //Intent intent = new Intent(RadioPlayActivity.this, RadioPlayActivity.class);
                //startActivity(intent);
                Toast.makeText(RadioPlayActivity.this, dataList.get(position).getJudulSiaran() + "clicked", Toast.LENGTH_SHORT).show();
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean checkTimeInRange(String time) {

        Calendar fromTime;
        Calendar toTime;
        Calendar currentTime;

        try{
            String[] times = time.split("-");
            String[] from = times[0].split(":");
            String[] until = times[1].split(":");

            fromTime = Calendar.getInstance();
            fromTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(from[0]));
            fromTime.set(Calendar.MINUTE, Integer.valueOf(from[1]));

            toTime = Calendar.getInstance();
            toTime.set(Calendar.HOUR_OF_DAY, Integer.valueOf(until[0]));
            toTime.set(Calendar.MINUTE, Integer.valueOf(until[1]));

            currentTime = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            currentTime.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
            currentTime.set(Calendar.HOUR_OF_DAY, currentTime.getTime().getHours());
            currentTime.set(Calendar.MINUTE, currentTime.getTime().getMinutes());

            if (currentTime.after(fromTime) && currentTime.before(toTime)) {
                Log.i("RadioPlayActivity", "Jam Sekarang In Range");
                return true;
            } else {
                Log.i("RadioPlayActivity", "Jam Sekarang Out Of Range ");
                return false;
            }

        } catch (Exception e) {
            Log.i("RadioPlayActivity", "The Error Is: " + e.toString());
        }

        return false;
    }



    private void startRadio() {
        final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                Log.d("RadioPlayActivity", "The url is = " + streamURL);
                radioManager.playOrPause(streamURL);
            }
        };

        handler.postDelayed(r, 500);
    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
        stopService(new Intent(RadioPlayActivity.this, RadioService.class));
    }

    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
        startService(new Intent(RadioPlayActivity.this, RadioService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        radioManager.unbind();
    }

    @Override
    protected void onResume() {
        super.onResume();

        radioManager.bind();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Subscribe
    public void onEvent(String status) {
        switch (status){
            case PlaybackStatus.LOADING:
                // loading
                break;

            case PlaybackStatus.ERROR:
                Toast.makeText(this, "Radio station couldn\'t stream!", Toast.LENGTH_SHORT).show();
                break;
        }

        buttonAudio.setImageResource(status.equals(PlaybackStatus.PLAYING)
                ? R.drawable.ic_pause_white
                : R.drawable.ic_play_white);
    }
}
