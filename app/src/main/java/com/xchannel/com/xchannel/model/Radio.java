package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Radio {
    @SerializedName("id_radio")
    @Expose
    private String idRadio;
    @SerializedName("nama_radio")
    @Expose
    private String namaRadio;
    @SerializedName("icon_radio")
    @Expose
    private String iconRadio;
    @SerializedName("url_radio")
    @Expose
    private String urlRadio;
    @SerializedName("api_jadwal")
    @Expose
    private String apiJadwal;

    public String getIdRadio() {
        return idRadio;
    }

    public String getNamaRadio() {
        return namaRadio;
    }

    public String getIconRadio() {
        return iconRadio;
    }

    public String getUrlRadio() {
        return urlRadio;
    }

    public String getApiJadwal() {
        return apiJadwal;
    }
}
