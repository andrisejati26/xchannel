package com.xchannel.com.xchannel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.model.Video;

import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ItemViewHolder> {
    private final List<Video> dataList;
    private final Context context;
    private final ClickHandler mClickHandler;

    public VideoAdapter(Context ctx, List<Video> data, ClickHandler clickHandler) {
        context = ctx;
        dataList = data;
        LayoutInflater mInflater = LayoutInflater.from(context);
        mClickHandler = clickHandler;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView image;
        private final TextView livestream;

        ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            image = itemView.findViewById(R.id.imageMain);
            livestream = itemView.findViewById(R.id.numItem);
        }

        @Override
        public void onClick(View v) {

            if (mClickHandler != null) mClickHandler.onItemClicked(getAdapterPosition());

        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_siaran, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Glide.with(context)
                .load(Constant.BASE_URL_IMAGE_VIDEO + dataList.get(position).getThumbVideo())
                .thumbnail(0.01f)
                .into(holder.image);

        holder.livestream.setText(dataList.get(position).getVideoTitle());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
