package com.xchannel.com.xchannel.ui.activity;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.session.SharedPrefManager;
import com.xchannel.com.xchannel.ui.fragment.LivestreamFragment;
import com.xchannel.com.xchannel.ui.fragment.RadioFragment;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private static final String[] pageTitle = {"Radio", "Video"};
    private DrawerLayout drawerLayout;
    NavigationView navigationView;
    private InterstitialAd mInterstitialAd;

    private SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepareads();

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            public void run() {
                Log.i("hello", "world");
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        } else {
                            Log.d("TAG", " Interstitial not loaded");
                        }
                        prepareads();
                    }
                });
            }
        }, 5, 5, TimeUnit.MINUTES);


    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) actionBar.setDisplayShowTitleEnabled(false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_activity3);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new WizardPageChangeListener());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.menu_profile:
                        Intent login = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(login);
                        return true;
                    case R.id.menu_video:
                        Intent i = new Intent(MainActivity.this,Activity_allvideo.class);
                        startActivity(i);
                        return true;
                    case R.id.menu_news:
                        Intent news = new Intent(MainActivity.this,news_activity.class);
                        startActivity(news);
                        return true;
                    case R.id.menu_logout:
                        signOut();
                        return true;
                    default:
                        return true;
                }
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        sharedPrefManager = new SharedPrefManager(this);

        // Set the profile in nav drawer
        View headerView = navigationView.getHeaderView(0);

        ImageView profileImage = headerView.findViewById(R.id.image_profile);
        TextView nameTextView = headerView.findViewById(R.id.name_textview);
        TextView emailTextView = headerView.findViewById(R.id.email_textview);

        Glide.with(this)
                .load(sharedPrefManager.getPhotoUrl())
                .thumbnail(0.01f)
                .into(profileImage);

        nameTextView.setText(sharedPrefManager.getName());
        emailTextView.setText((sharedPrefManager.getEmail().equalsIgnoreCase("-")) ? sharedPrefManager.getPhoneNumber() : sharedPrefManager.getEmail());


        Log.i(TAG, "Phone number is: " + sharedPrefManager.getPhoneNumber());

    }

    private void prepareads() {

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }



    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final int WIZARD_PAGES_COUNT = 2;

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new RadioFragment(position);
                case 1:
                   return new LivestreamFragment(position);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return WIZARD_PAGES_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pageTitle[position];
        }
    }

    private class WizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Toast.makeText(this, "action clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!sharedPrefManager.getIsSignedIn()) startActivity(new Intent(this, SignInActivity.class));

        Log.i("ProfileActivity", sharedPrefManager.getName());
        Log.i("ProfileActivity", String.valueOf(sharedPrefManager.getIsSignedIn()));
        Log.i("ProfileActivity", String.valueOf(sharedPrefManager.getMethod()));
    }

    private void signOut() {

        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        sharedPrefManager.saveSharedPrefBoolean(SharedPrefManager.IS_SIGNEDIN_KEY, false);
        startActivity(new Intent(MainActivity.this, SignInActivity.class));
        finish();

    }

}
