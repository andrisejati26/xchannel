package com.xchannel.com.xchannel.networking;

import com.xchannel.com.xchannel.model.Live;
import com.xchannel.com.xchannel.model.Radio;
import com.xchannel.com.xchannel.model.RadioSchedule;
import com.xchannel.com.xchannel.model.Siaran;
import com.xchannel.com.xchannel.model.Video;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface GetDataService {
    @GET("api/api.php?list_radio")
    Call<List<Radio>> getAllRadio();

    @GET("api/api.php?list_video")
    Call<List<Video>> getAllVideo();

    @GET("api/api.php?jadwal_siaran_video")
    Call<List<Siaran>> getAllSiaran();

    @GET("api/api.php?live_stream")
    Call<List<Live>> getAllStream();

    @GET
    Call<List<RadioSchedule>> getAllRadioSchedule(@Url String url);
}
