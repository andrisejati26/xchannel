package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RadioSchedule {

    @SerializedName("id_siaran_bandung")
    @Expose
    private String idSiaranBandung;
    @SerializedName("judul_siaran")
    @Expose
    private String judulSiaran;
    @SerializedName("hari_siaran")
    @Expose
    private String hariSiaran;
    @SerializedName("waktu_siaran")
    @Expose
    private String waktuSiaran;
    @SerializedName("deskripsi_siaran")
    @Expose
    private String deskripsiSiaran;
    @SerializedName("thumb_siaran")
    @Expose
    private String thumbSiaran;

    public String getIdSiaranBandung() {
        return idSiaranBandung;
    }

    public void setIdSiaranBandung(String idSiaranBandung) {
        this.idSiaranBandung = idSiaranBandung;
    }

    public String getJudulSiaran() {
        return judulSiaran;
    }

    public void setJudulSiaran(String judulSiaran) {
        this.judulSiaran = judulSiaran;
    }

    public String getHariSiaran() {
        return hariSiaran;
    }

    public void setHariSiaran(String hariSiaran) {
        this.hariSiaran = hariSiaran;
    }

    public String getWaktuSiaran() {
        return waktuSiaran;
    }

    public void setWaktuSiaran(String waktuSiaran) {
        this.waktuSiaran = waktuSiaran;
    }

    public String getDeskripsiSiaran() {
        return deskripsiSiaran;
    }

    public void setDeskripsiSiaran(String deskripsiSiaran) {
        this.deskripsiSiaran = deskripsiSiaran;
    }

    public String getThumbSiaran() {
        return thumbSiaran;
    }

    public void setThumbSiaran(String thumbSiaran) {
        this.thumbSiaran = thumbSiaran;
    }

}