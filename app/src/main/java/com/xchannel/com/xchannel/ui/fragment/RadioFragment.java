package com.xchannel.com.xchannel.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.adapter.FragmentRadioAdapter;
import com.xchannel.com.xchannel.model.Radio;
import com.xchannel.com.xchannel.networking.GetDataService;
import com.xchannel.com.xchannel.networking.RetrofitClientInstance;
import com.xchannel.com.xchannel.ui.activity.RadioPlayActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class RadioFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView rView;
    private TextView noInternet;
    private ConstraintLayout Navplay;

    private MediaPlayer mediaPlayer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    public RadioFragment() { }

    public RadioFragment(int position) {
        int wizard_page_position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layout_id = R.layout.fragment_radio;
        View view = inflater.inflate(layout_id, container, false);

        GridLayoutManager lLayout = new GridLayoutManager(getActivity(), 2);

        rView = view.findViewById(R.id.recyclerView);
        rView.setHasFixedSize(false);
        rView.setLayoutManager(lLayout);
        rView.setNestedScrollingEnabled(false);
        rView.setHasFixedSize(true);

        noInternet = view.findViewById(R.id.textView2);
        Navplay = view.findViewById(R.id.toolbar_radio);
        Navplay.setVisibility(View.INVISIBLE);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                loadData();
            }
        });

        AudioManager mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager.isMusicActive()) Navplay.setVisibility(View.VISIBLE);

        return view;
    }

    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if(focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT || focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
                // mediaPlayer.pause();
                // mediaPlayer.seekTo(0);
                Navplay.setVisibility(View.VISIBLE);
            } else if(focusChange == AudioManager.AUDIOFOCUS_GAIN){
                // mediaPlayer.start();
                Navplay.setVisibility(View.VISIBLE);
            } else if(focusChange == AudioManager.AUDIOFOCUS_LOSS){
                // releaseMediaPlayer();
                Navplay.setVisibility(View.VISIBLE);
            }
        }
    };

    private void loadData() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<Radio>> call = service.getAllRadio();
        call.enqueue(new Callback<List<Radio>>() {
            @Override
            public void onResponse(Call<List<Radio>> call, Response<List<Radio>> response) {
                rView.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);
                generateDataList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<Radio>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);
            }
        });
    }

    private void generateDataList(final List<Radio> dataList) {
        FragmentRadioAdapter mAdapter = new FragmentRadioAdapter(getActivity(), dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {

                Toast.makeText(getActivity(), "Id Radio = " + dataList.get(position).getIdRadio(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getActivity(), RadioPlayActivity.class);
                intent.putExtra("streamurl", dataList.get(position).getUrlRadio());
                intent.putExtra("pathradio", dataList.get(position).getApiJadwal());
                startActivity(intent);
            }
        });
        rView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
