package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Video {
    @SerializedName("id_video")
    @Expose
    private String idVideo;
    @SerializedName("video_title")
    @Expose
    private String videoTitle;
    @SerializedName("thumb_video")
    @Expose
    private String thumbVideo;
    @SerializedName("video_type")
    @Expose
    private String videoType;
    @SerializedName("url_video")
    @Expose
    private String urlVideo;
    @SerializedName("deskripsi_video")
    @Expose
    private String deskripsiVideo;

    public String getIdVideo() {
        return idVideo;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public String getThumbVideo() {
        return thumbVideo;
    }

    public String getVideoType() {
        return videoType;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getDeskripsiVideo() {
        return deskripsiVideo;
    }
}
