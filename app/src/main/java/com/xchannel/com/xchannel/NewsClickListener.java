package com.xchannel.com.xchannel;

import android.view.View;

public interface NewsClickListener {
    void itemClicked(View view, int position);
}